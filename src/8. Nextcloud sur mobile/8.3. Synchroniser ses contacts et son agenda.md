Synchroniser ses contacts et agendas
====================================

Synchroniser ses contacts et agendas sur Android
------------------------------------------------

Nextcloud ne propose pas spécifiquement d'application mobile pour les
contacts et les agendas. Il est cependant possible de les synchroniser
avec vos propres applications **sur Android**, en utilisant
<a href="https://www.davx5.com/" title="site en anglais" hreflang="en">Davx5</a>.

1.  Recherchez l'application "DAVx5" dans votre magasin habituel
    d'applications.

    ![](application-davx-android.png)

    -   DAVx5 est gratuit quand il est installé depuis le magasin
        d'applications F-droid et payant depuis les autres magasins
        d'applications, afin de soutenir ce projet open source.

2.  Installez l'application puis ouvrez-la.

3.  Passez l'écran d'accueil et l'écran de gestion des tâches en appuyant sur la flèche en bas à droite.

    <p><img src="passer-ecrans.png" alt="" width="300"/></p>

4.  Autorisez la synchronisation des contacts et/ou des calendriers
    selon vos besoins.

    <p><img src="autorisations-davx.png" alt="trois actions possibles à cocher : tout autoriser, autorisation d'accès aux contacts, autorisations du calandrier." width="300"/></p>

5.  Pour chaque, autorisez l'accès à votre mobile dans la boîte de dialogue qui apparaît.

    ![](acces-davx.png)

6.  Passez l'écran d'autorisations puis l'écran d'information sur
    l'application en cliquant sur a flèche en bas à droite.

7.  Cliquez sur le bouton **+** en bas à droite pour créer un nouveau
    compte.

    <p><img src="ajouter-compte.png" alt="" width="300"/></p>

8.  Choisissez "Connexion avec une URL et un nom d'utilisateur", entrez
    vos informations de connexion au Nextcloud de Coopaname et cliquez
    sur "Se connecter" en bas à droite.

    <p><img src="connexion-nextcloud-davx.png" alt="" width="300"/></p>

9.  Cliquez sur "Créer un compte" en bas à droite de la nouvelle page.

    <p><img src="creer-compte.png" alt="" width="300"/></p>

10. Dans les onglets correspondants, cochez les carnets d'adresse et les agendas Nextcloud que vous souhaitez
    ajouter à vos applications mobile de contacts et de calendrier.

    ![](selection-synchronisation.png)

> ℹ️ Pour que les contacts et les événements sur votre mobile
> apparaissent dans votre espace Nextcloud, créez ou copiez-les sur le
> compte Davx désormais proposé dans votre application de contact ou de
> calendrier.

> ℹ️ La synchronisation prend du temps, parfois plusieurs minutes. Si
> des contacts ou des événements n'apparaissent pas sur votre espace
> Nextcloud ou sur votre mobile, patientez une dizaine de minutes et
> rafraîchissez les pages.

Synchroniser ses contacts et agendas sur iOS
--------------------------------------------

> ⚠️ Cette partie de la documentation est théorique, il faut encore la
> tester.

### Synchroniser ses contacts

1.  Ouvrir *Réglages -\> Contacts -\> Comptes -\> Ajouter un compte -\>
    Autre -\> Ajouter un compte CardDAV*
2.  Entrez l'adresse nextcloud.coopaname.coop et vos informations de
    connexion au Nextcloud de Coopaname
3.  Validez.

### Synchroniser ses agendas

Ouvrir *Réglages -\> Contacts -\> Comptes -\> Ajouter un compte -\>
Autre -\> Ajouter un compte CalDAV*

1.  Entrez l'adresse nextcloud.coopaname.coop et vos informations de
    connexion au Nextcloud de Coopaname
2.  Validez.

