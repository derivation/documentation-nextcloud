# Contacts

Le module **Contacts** permet d'organiser ses contacts et de partager
des carnets d'adresse avec d'autres personnes inscrites sur votre instance Nextcloud. Il permet aussi de créer des **Cercles**, utiles pour
organiser du travail collectif dans les autres modules de Nextcloud
(Collectifs, Fichiers, Agenda...).

> 📗 Le vocabulaire employé par le module peut prêter à confusion. Voici
> quelques définitions :
>
> -   **Contact** = une entrée dans votre répertoire personnel, à
>     renseigner à la main ou à importer depuis un autre logiciel.
> -   **Groupe** = Un mode d'organisation de ses contacts personnels.
>     Les groupes ne sont pas publics.
> -   **Carnet d'adresse** = Un autre mode d'organisation de ses
>     contacts, qui permet le partage de contacts avec des
>     utilisateur⋅ices.
> -   **Utilisateur⋅ice** = Un ⋅e membres de Coopaname inscrit⋅e sur
>     votre instance Nextcloud. Vous pouvez les retrouver dans
>     l'annuaire en haut à droite de l'interface générale, si ceux-ci ont été rendus publics.\
>     ![l'annuaire est le troisième bouton dans le groupe à droite de la barre d'accès](annuaire.png)
> -   **Cercle** = Un outil pour créer des groupes d'utilisateur⋅ices.
>     Ils peuvent être privés ou publics. Les membres savent qu'ils sont
>     dans un cercle, et le cercle apparaît dans leur propre liste de
>     contacts.

------------------------------------------------------------------------

➡️ Continuer en [créant un
contact](<../6. Contacts/6.1. Créer un contact.md>).
