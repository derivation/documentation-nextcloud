# Discuter autour d’un document

## Partager un document

Il y a deux façons de partager un document dans une conversation.

* Cliquez-glissez le document directement dans la conversation.
* Cliquez sur le trombone à gauche de la zone d'écriture.

  ![](joindre-document.png)
   Le menu déroulant propose :
  * Envoyer de nouveaux fichiers -> depuis votre ordinateur.
  * Partager vos fichiers déjà stockés -> depuis votre espace personnel sur Nextcloud.

Vous pouvez ajouter plusieurs fichiers en une seule fois.

Tou⋅tes les membres de la discussion pourront **voir, éditer ou télécharger** le document, qu'ils ou elles soient inscrit⋅es sur l'instance ou venant de l'extérieur.

## Lier une discussion à un document

Dans le module **Fichiers**, vous pouvez démarrer une discussion écrite ou vidéo **depuis la barre latérale** de chaque document pendant que vous l'éditez.

1. Ouvrez un document.
2. Cliquer sur le menu d'actions en haut à droite de la page pour ouvrir la barre latérale.

   ![](ouvrir-barre-laterale.png)
3. Cliquez sur l'onglet "Tchat".
4. Cliquez sur le bouton "Rejoindre la conversation".

   ![](discussion-fichier-barre-laterale.png)
5. Tapez votre message dans la zone d'écriture ou cliquez sur "Commencer l'appel".

Vous pouvez aussi rejoindre la discussion **depuis la liste des documents** :

1. Cliquez sur le menu de partage pour ouvrir la barre latérale
2. Cliquez sur l'onglet "Tchat"
3. Cliquez sur le bouton "Rejoindre la conversation"

   ![](discussion-fichier-liste.png)
4. Tapez votre message dans la zone d'écriture ou cliquez sur "Commencer l'appel".

La conversation ainsi créée apparaît dans la **liste du module Discussion**. Vous pouvez y ajuster les paramètres en cliquant sur le menu d'action correspondant.

![](parametres-conversation.png)

---

➡️ Continuer en utilisant les [collectifs](<../5. Collectifs/Readme.md>).
