# Discuter

Le module **Discussion** permet de discuter par écrit (conversation) ou en vidéo (appel) sans quitter Nextcloud.

![](discussion.png)

Il existe deux types de discussion :

1. **les discussion privées,** avec une seule autre personne. Ce type de discussion ne peut pas être partagé à d'autres personnes. Il n'est pas possible de discuter avec quelqu'un n'ayant pas un compte sur votre instance.
2. **les discussions de groupe,** avec autant de personnes que nécessaire. Ce type de discussion peut être partagé à des personnes n'ayant pas de compte sur votre instance. Il est aussi possible de les rendre publiques, pour que d'autres personnes inscrites sur l'instance puissent s'y joindre spontanément.

---

➡️ Continuer en [créant une discussion](<4.1. Créer une discussion.md>).
