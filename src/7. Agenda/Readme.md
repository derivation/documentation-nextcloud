Agenda
======

Le module **Agenda** permet d'organiser ses rendez-vous et événements et
de les partager avec d'autres personnes en interne ou à l'extérieur.

Tour d'horizon de l'interface :

![](interface-agenda.png)

1.  Les événements sont affichés dans la **vue principale**.
2.  **Indicateur** de la vue affichée se trouve en haut du menu à gauche. Il permet de **naviguer** vers
    une autre journée, semaine ou mois.
3.  Sous l'indicateur, un bouton permet de créer un **nouvel événement**.
4.  À côté, le bouton "Auj" permet de **recentrer la vue** sur la journée, la semaine ou
    le mois en cours.
5.  En bout de ligne, une icône permet de choisir le **type de vue** : journée, semaine, mois (comme ici) ou liste des événements.
6.  Le reste du menu contient la **liste des agendas** auxquels vous avez accès (les vôtres et ceux
    qui vous sont partagés).
7.  Tout en bas, vous avez accès à la **corbeille** et aux **paramètres** pour ce module.

------------------------------------------------------------------------

➡️ Continuer en voyant comment [gérer les
agendas](<../7. Agenda/7.1. Gérer ses agendas.md>).

